Source: rust-findutils
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-cargo (>= 25),
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-chrono-0.4+default-dev,
 librust-clap-4+default-dev (>= 4.4-~~),
 librust-faccess-0.2+default-dev (>= 0.2.4-~~),
 librust-nix-0+default-dev (>= 0.27-~~),
 librust-nix-0+fs-dev (>= 0.27-~~),
 librust-nix-0+user-dev (>= 0.27-~~),
 librust-once-cell-1+default-dev (>= 1.19-~~),
 librust-onig-6-dev,
 librust-regex-1+default-dev (>= 1.10-~~),
 librust-uucore+default-dev (>= 0.0.22-~~),
 librust-uucore+entries-dev (>= 0.0.22-~~),
 librust-uucore+fs-dev (>= 0.0.22-~~),
 librust-uucore+fsext-dev (>= 0.0.22-~~),
 librust-uucore+mode-dev (>= 0.0.22-~~),
 librust-walkdir-2+default-dev (>= 2.4-~~)
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/findutils]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/findutils
Homepage: https://github.com/uutils/findutils
X-Cargo-Crate: findutils
Rules-Requires-Root: no

Package: librust-findutils-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-chrono-0.4+default-dev,
 librust-clap-4+default-dev (>= 4.4-~~),
 librust-faccess-0.2+default-dev (>= 0.2.4-~~),
 librust-nix-0+default-dev (>= 0.27-~~),
 librust-nix-0+fs-dev (>= 0.27-~~),
 librust-nix-0+user-dev (>= 0.27-~~),
 librust-once-cell-1+default-dev (>= 1.19-~~),
 librust-onig-6-dev,
 librust-regex-1+default-dev (>= 1.10-~~),
 librust-uucore+default-dev (>= 0.0.22-~~),
 librust-uucore+entries-dev (>= 0.0.22-~~),
 librust-uucore+fs-dev (>= 0.0.22-~~),
 librust-uucore+fsext-dev (>= 0.0.22-~~),
 librust-uucore+mode-dev (>= 0.0.22-~~),
 librust-walkdir-2+default-dev (>= 2.4-~~)
Provides:
 librust-findutils+default-dev (= ${binary:Version}),
 librust-findutils-0-dev (= ${binary:Version}),
 librust-findutils-0+default-dev (= ${binary:Version}),
 librust-findutils-0.7-dev (= ${binary:Version}),
 librust-findutils-0.7+default-dev (= ${binary:Version}),
 librust-findutils-0.7.0-dev (= ${binary:Version}),
 librust-findutils-0.7.0+default-dev (= ${binary:Version})
Description: GNU findutils - Rust source code
 Source code for Debianized Rust crate "findutils"

Package: rust-findutils
Architecture: any
Multi-Arch: allowed
Section: utils
Priority: required
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides},
 findutils
Conflicts:
 findutils,
 findutils-gplv2
Replaces:
 findutils,
 findutils-gplv2
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Rust implementation of GNU findutils
 Propose an dropped in replacement for find, xargs, etc.
